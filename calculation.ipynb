{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Calculating the position of the test stand and IP\n",
    "\n",
    "In order to understand what tracks left by muons coming from the IP _should_ look like in data, we need to know where the IP is relative to the test stand. In brief, the strategy is:\n",
    "\n",
    "- Calculate the horizontal position of the test stand relative to some surface reference point.\n",
    "- Calculate the horizontal position of the IP projected vertically to the surface relative to the same reference point.\n",
    "- Find the vertical elevation of the test stand and the IP.\n",
    "\n",
    "By taking the difference of the positions of the test stand and IP and applying the right rotational transformation, we then get either the IP position in the test stand reference frame or the test stand position in the ATLAS reference frame."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Horizontal positions"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is the test stand sitting in the buffer zone during June 2018:\n",
    "\n",
    "![Buffer zone photo](buffer_zone_camera.jpg)\n",
    "(Image courtesy of Cristiano)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's zoom in on the test stand. I've marked and labeled a few important lines:\n",
    "\n",
    "![Labeled buffer zone photo](buffer_zone_labeled.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In order to get the position, we need to do some calculations using these lines. First, set up the `pint` package for carrying around units and uncertainties."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pint\n",
    "import math\n",
    "ureg = pint.UnitRegistry()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "I measured the lengths of AB, BC, BD, and CD in pixels in the image. I also estimated the uncertainty of each endpoint. These are the results:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "AB = (115.0 +/- 0.4) pixel\n",
      "AC = (180.5 +/- 0.6) pixel\n",
      "BD = (180.5 +/- 0.6) pixel\n",
      "CD = (111.0 +/- 0.4) pixel\n"
     ]
    }
   ],
   "source": [
    "AB = (115.0 * ureg.pixel).plus_minus(math.hypot(1.0 / math.sqrt(12.0), 1.0 / math.sqrt(12.0)))\n",
    "AC = (180.5 * ureg.pixel).plus_minus(math.hypot(1.0 / math.sqrt(12.0), 2.0 / math.sqrt(12.0)))\n",
    "BD = (180.5 * ureg.pixel).plus_minus(math.hypot(1.0 / math.sqrt(12.0), 2.0 / math.sqrt(12.0)))\n",
    "CD = (111.0 * ureg.pixel).plus_minus(math.hypot(1.0 / math.sqrt(12.0), 1.0 / math.sqrt(12.0)))\n",
    "print('AB =', AB)\n",
    "print('AC =', AC)\n",
    "print('BD =', BD)\n",
    "print('CD =', CD)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We need to know the physical length of something in the picture for scale. This can be determined from this diagram of the buffer zone:\n",
    "\n",
    "![Buffer zone diagram](buffer_zone_diagram.png)\n",
    "(Image courtesy of Cristiano again)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now I'll rotate the diagram and mark approximately where the test stand is in purple to better illustrate what we're seeing in the photo.\n",
    "\n",
    "![Buffer zone diagram with test stand position](buffer_zone_diagram_ts.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The length of AB and CD should be (16210. - 3540. - 3000. - 3000. - 3670.) mm = 3000. mm, as shown on the left side of the diagram."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "AB, CD = (3000.0 ± 0.6) mm\n"
     ]
    }
   ],
   "source": [
    "AB_CD_distance = (3000.0 * ureg.millimeter).plus_minus(math.sqrt(5.0) * 1.0 / math.sqrt(12.0))\n",
    "print('AB, CD = {:~P}'.format(AB_CD_distance))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can calculate the scale on the photo and get the horizontal position of the left edge of the test stand."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Image scale = (26.55 +/- 0.07) millimeter / pixel\n"
     ]
    }
   ],
   "source": [
    "image_scale = AB_CD_distance / ((AB + CD) / 2.0)\n",
    "print('Image scale =', image_scale)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Horizontal distance from left edge of buffer zone to left edge of test stand (length of AC and BD) = (4792 ± 17) mm\n"
     ]
    }
   ],
   "source": [
    "distance_BZ_left_edge_to_TS_left_edge = (AC + BD) / 2.0 * image_scale\n",
    "print('Horizontal distance from left edge of buffer zone to left edge of test stand (length of AC and BD) = {:~P}'.format(distance_BZ_left_edge_to_TS_left_edge))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Technically, what we really want is the position of the center of the test stand because that's where the local origin is (and it's just more convenient). To convert to this, we just add half the length of the test stand along the left-right axis here."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Horizontal distance from left edge of buffer zone to center of test stand = (6280 ± 17) mm\n"
     ]
    }
   ],
   "source": [
    "# These measurements are from https://gitlab.cern.ch/mathusla/TestStandGeometry\n",
    "substructure_a_horizontal_length_sides_2_and_4 = \\\n",
    "  (   (2.977 * ureg.meter).plus_minus(0.0012)\n",
    "    + (2.974 * ureg.meter).plus_minus(0.0012)\n",
    "    + (2.976 * ureg.meter).plus_minus(0.0012)\n",
    "  ) / 3.0\n",
    "distance_BZ_left_edge_to_TS_center = distance_BZ_left_edge_to_TS_left_edge + substructure_a_horizontal_length_sides_2_and_4 / 2.0\n",
    "print('Horizontal distance from left edge of buffer zone to center of test stand = {:~P}'.format(distance_BZ_left_edge_to_TS_center))"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we need the location of the ATLAS IP. The horizontal position is set by the intersection of two vertical planes: one bisecting both the PX14 and PX16 shafts, and the other bisecting both the PX15 and PM15 shafts. Look for the little plus at the center of the bottom-right diagram here:\n",
    "\n",
    "![Point 1 shafts diagram](shafts.png)\n",
    "(Image from the ATLAS Technical Co-ordination TDR)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To determine the projection onto the surface, first take this drawing of SX1, the building housing the buffer zone:\n",
    "\n",
    "![SX1 drawing](sx1_drawing.png)\n",
    "(Another image from the ATLAS Technical Co-ordination TDR)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that there are already lines through the centers of all four of the shafts (even though PX15 and PM15 aren't actually on the diagram). Again, I've zoomed in and rotated to better reflect what we see in the photo. The purple box still represents the footprint of the test stand, and the red star is the horizontal position of the IP.\n",
    "\n",
    "![SX1 drawing with test stand position](sx1_drawing_matched_first.png)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To get a common reference for both the test stand and the IP, I superimpose and match up the buffer zone diagram onto the above one for SX1 by lining up the locations of the building supports (outlined in red below). I've drawn an orange line through one pair of these supports at the top and bottom, which is a centerline of SX1.\n",
    "\n",
    "![SX1 drawing with buffer zone overlay](sx1_drawing_matched_second.png)\n",
    "\n",
    "Note that the horizontal position of the IP is marked as 4200 mm left of the centerline. Thus, if we calculate the position of the orange line relative to the left edge of the buffer zone, we will be able to get the test stand position relative to the IP."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Zooming in once more and adding points E and F where the orange line would intersect the extensions of lines AC and BD, respectively, we get:\n",
    "\n",
    "![SX1 drawing zoomed in](sx1_drawing_matched_third.png)\n",
    "\n",
    "I measured the length of AE (or equivalently, BF) in pixels in the original buffer zone diagram."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "AE, BF = (231.0 +/- 0.4) pixel\n"
     ]
    }
   ],
   "source": [
    "AE_BF = (805.5 * ureg.pixel).plus_minus(1.0 / math.sqrt(12.0)) - (574.5 * ureg.pixel).plus_minus(1.0 / math.sqrt(12.0))\n",
    "print('AE, BF =', AE_BF)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Just like with the photo, I need to convert this pixel measurement by calculating the scale of the diagram. I used the largest given length measurement (refer back the full diagram first shown) to minimize the uncertainty."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Buffer zone diagram scale: (35.036 +/- 0.026) millimeter / pixel\n"
     ]
    }
   ],
   "source": [
    "largest_length_in_px = (836.5 * ureg.pixel).plus_minus(1.0 / math.sqrt(12.0)) - (288.5 * ureg.pixel).plus_minus(1.0 / math.sqrt(12.0))\n",
    "largest_length_in_mm = (19200.0 * ureg.millimeter).plus_minus(1.0 / math.sqrt(12.0))\n",
    "buffer_zone_diagram_scale = largest_length_in_mm / largest_length_in_px\n",
    "print('Buffer zone diagram scale:', buffer_zone_diagram_scale)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "AE, BF = (8093 ± 16) mm\n"
     ]
    }
   ],
   "source": [
    "AE_BF_distance = AE_BF * buffer_zone_diagram_scale\n",
    "print('AE, BF = {:~P}'.format(AE_BF_distance))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Transform this so it's measured from the common reference:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Horizontal distance from left edge of buffer zone to IP = (3893 ± 16) mm\n"
     ]
    }
   ],
   "source": [
    "distance_BZ_left_edge_to_IP = AE_BF_distance - (4200.0 * ureg.millimeter).plus_minus(1.0 / math.sqrt(12.0))\n",
    "print('Horizontal distance from left edge of buffer zone to IP = {:~P}'.format(distance_BZ_left_edge_to_IP))"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "At this point, I'm going to make the choice to work in a coordinate system called 'CERN axis civil engineering' (axes in green) in the following diagram:\n",
    "\n",
    "![LHC coordinate systems](lhc_coordinates.png)\n",
    "\n",
    "- The origin is the ATLAS IP.\n",
    "- The *x*-axis is horizontal and points towards the center of the LHC ring.\n",
    "- The *y*-axis is also horizontal and follows the vertical projection of the beam axis clockwise.\n",
    "- The *z*-axis points vertically upward.\n",
    "\n",
    "Note that these axes are different from both the ATLAS coordinate system and the test stand coordinate system. I will convert between them as necessary as the very last step."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is what these horizontal axes look like in our diagram:\n",
    "\n",
    "![CACE axes](cace_axes.png)\n",
    "\n",
    "For the _y_<sub>CACE</sub>-coordinate of the test stand, we take the distance from the left edge of the buffer zone to the horizontal center of the test stand and subtract the horizontal distance from the same edge to the IP."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "CACE y-coordinate of test stand center: (2386 ± 23) mm\n"
     ]
    }
   ],
   "source": [
    "test_stand_y_cace = distance_BZ_left_edge_to_TS_center - distance_BZ_left_edge_to_IP\n",
    "print('CACE y-coordinate of test stand center: {:~P}'.format(test_stand_y_cace))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For *x*, I estimate that the test stand was approximately centered in the 3 meter-wide trench in this direction. This puts it along the horizontal projection of the beam axis (_x_<sub>CACE</sub> = 0). I derive the uncertainty from difference between the length of the bottom of the test stand in this direction and the width of the trench (AB, CD, EF)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "CACE x-coordinate of test stand center: (0 ± 25) mm\n"
     ]
    }
   ],
   "source": [
    "# These measurements are again from https://gitlab.cern.ch/mathusla/TestStandGeometry\n",
    "substructure_a_horizontal_length_sides_1_and_3 = \\\n",
    "  (   (2.912 * ureg.meter).plus_minus(0.0012)\n",
    "    + (2.912 * ureg.meter).plus_minus(0.0012)\n",
    "  ) / 2.0\n",
    "x_total_gap = AB_CD_distance - substructure_a_horizontal_length_sides_1_and_3\n",
    "test_stand_x_cace = (0.0 * ureg.millimeter).plus_minus(x_total_gap.to(ureg.millimeter).value.magnitude / math.sqrt(12.0))\n",
    "print('CACE x-coordinate of test stand center: {:~P}'.format(test_stand_x_cace))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Done with the horizontal coordinates! (The IP is at x<sub>CACE</sub> = y<sub>CACE</sub> = 0 by definition.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Vertical positions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you go back up and look at the SX1 plan, the vertical level of the trench where the test stand sat was at -1.600 m below the standard floor reference. On the side of the diagram (in the TDR but not shown above), the reference level is stated to be an elevation above sea level (ASL) of 440.250 m."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Buffer zone trench elevation: (438.6500 ± 0.0004) m\n"
     ]
    }
   ],
   "source": [
    "sx1_floor_elevation = (440.250 * ureg.meter).plus_minus(0.001 / math.sqrt(12.0))\n",
    "trench_elevation = sx1_floor_elevation + (-1.600 * ureg.meter).plus_minus(0.001 / math.sqrt(12.0))\n",
    "print('Buffer zone trench elevation: {:~P}'.format(trench_elevation))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The elevation of the center of the test stand can be determined by adding all the substructure heights and dividing by 2:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Elevation of center of test stand: (442.0481 ± 0.0010) m\n"
     ]
    }
   ],
   "source": [
    "# These measurements are again from https://gitlab.cern.ch/mathusla/TestStandGeometry\n",
    "# This includes both the substructure heights and the gaps between them (which I estimated to all be zero, but this accounts for the uncertainty)\n",
    "test_stand_structure_height = \\\n",
    "  (\n",
    "      (   (1.669 * ureg.meter).plus_minus(0.0012)\n",
    "        + (1.669 * ureg.meter).plus_minus(0.0012)\n",
    "        + (1.671 * ureg.meter).plus_minus(0.0012)\n",
    "        + (1.671 * ureg.meter).plus_minus(0.0012)\n",
    "      ) / 4.0\n",
    "    + (   (0.000 * ureg.meter).plus_minus(0.0012)\n",
    "        + (0.000 * ureg.meter).plus_minus(0.0012)\n",
    "        + (0.000 * ureg.meter).plus_minus(0.0012)\n",
    "        + (0.000 * ureg.meter).plus_minus(0.0012)\n",
    "      ) / 4.0\n",
    "    + (   (1.735 * ureg.meter).plus_minus(0.0012)\n",
    "        + (1.735 * ureg.meter).plus_minus(0.0012)\n",
    "        + (1.737 * ureg.meter).plus_minus(0.0012)\n",
    "        + (1.738 * ureg.meter).plus_minus(0.0012)\n",
    "      ) / 4.0\n",
    "    + (   (0.000 * ureg.meter).plus_minus(0.0012)\n",
    "        + (0.000 * ureg.meter).plus_minus(0.0012)\n",
    "        + (0.000 * ureg.meter).plus_minus(0.0012)\n",
    "        + (0.000 * ureg.meter).plus_minus(0.0012)\n",
    "      ) / 4.0\n",
    "    + (   (1.735 * ureg.meter).plus_minus(0.0012)\n",
    "        + (1.738 * ureg.meter).plus_minus(0.0012)\n",
    "        + (1.736 * ureg.meter).plus_minus(0.0012)\n",
    "        + (1.736 * ureg.meter).plus_minus(0.0012)\n",
    "      ) / 4.0\n",
    "    + (   (0.000 * ureg.meter).plus_minus(0.0012)\n",
    "        + (0.000 * ureg.meter).plus_minus(0.0012)\n",
    "        + (0.000 * ureg.meter).plus_minus(0.0012)\n",
    "        + (0.000 * ureg.meter).plus_minus(0.0012)\n",
    "      ) / 4.0\n",
    "    + (   (1.036 * ureg.meter).plus_minus(0.0012)\n",
    "        + (1.036 * ureg.meter).plus_minus(0.0012)\n",
    "        + (1.037 * ureg.meter).plus_minus(0.0012)\n",
    "        + (1.035 * ureg.meter).plus_minus(0.0012)\n",
    "      ) / 4.0\n",
    "    + (   (0.000 * ureg.meter).plus_minus(0.0012)\n",
    "        + (0.000 * ureg.meter).plus_minus(0.0012)\n",
    "        + (0.000 * ureg.meter).plus_minus(0.0012)\n",
    "        + (0.000 * ureg.meter).plus_minus(0.0012)\n",
    "      ) / 4.0\n",
    "    + (   (0.618 * ureg.meter).plus_minus(0.0012)\n",
    "        + (0.618 * ureg.meter).plus_minus(0.0012)\n",
    "        + (0.618 * ureg.meter).plus_minus(0.0012)\n",
    "        + (0.617 * ureg.meter).plus_minus(0.0012)\n",
    "      ) / 4.0\n",
    "  )\n",
    "test_stand_center_elevation = trench_elevation + test_stand_structure_height / 2.0\n",
    "print('Elevation of center of test stand: {:~P}'.format(test_stand_center_elevation))"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then we just need the elevation of the IP. Here's a plan of UX15, the ATLAS experiment cavern:\n",
    "\n",
    "![UX15 drawing](ux15_drawing.png)\n",
    "\n",
    "The normal floor (base slab) elevation is 347.669 m."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The height of the IP from the floor is 11,370 mm, as shown in this diagram for some of the scaffolding in the cavern:\n",
    "\n",
    "![UX15 scaffolding diagram](longitudinal_beam.png)\n",
    "(Taken from https://edms.cern.ch/ui/#!master/navigator/document?D:1026558858:1026558858:subDocs)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Elevation of IP: (359.0390 ± 0.0004) m\n"
     ]
    }
   ],
   "source": [
    "ux15_floor_elevation = (347.669 * ureg.meter).plus_minus(0.001 / math.sqrt(12.0))\n",
    "ip_elevation = ux15_floor_elevation + (11370.0 * ureg.millimeter).plus_minus(1.0 / math.sqrt(12.0))\n",
    "print('Elevation of IP: {:~P}'.format(ip_elevation))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here I want to point out that this uncertainty for the elevation is necessarily too small. The cavern floor has shown movements of up to 4 mm in height between August 2003 and December 2018 (https://edms.cern.ch/ui/file/2087110/1/STAB_FloorDec2018.pdf) in some spots. One could imagine that the same could easily happen with the floors of SX1. Therefore I manually reset the uncertainty in the elevations of the test stand and the IP. I would apply this to the horizontal positions as well, but the uncertainties there are already much larger (~cm)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Elevation of center of test stand: (442.048 ± 0.004) m\n",
      "Elevation of IP: (359.039 ± 0.004) m\n"
     ]
    }
   ],
   "source": [
    "test_stand_center_elevation = test_stand_center_elevation.value.plus_minus(0.004)\n",
    "ip_elevation = ip_elevation.value.plus_minus(0.004)\n",
    "print('Elevation of center of test stand: {:~P}'.format(test_stand_center_elevation))\n",
    "print('Elevation of IP: {:~P}'.format(ip_elevation))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Just take the difference to get the *z*-position in the CACE coordinate system:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "CACE z-coordinate of test stand center: (83.009 ± 0.006) m\n"
     ]
    }
   ],
   "source": [
    "test_stand_z_cace = test_stand_center_elevation - ip_elevation\n",
    "print('CACE z-coordinate of test stand center: {:~P}'.format(test_stand_z_cace))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## CERN axis civil engineering (CACE) coordinate system\n",
    "To summarize, for the test stand center in the CACE coordinate system, we have:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Test stand center in CACE coordinates:\n",
      "x = (0.000 ± 0.025) m\n",
      "y = (2.386 ± 0.023) m\n",
      "z = (83.009 ± 0.006) m\n"
     ]
    }
   ],
   "source": [
    "print('Test stand center in CACE coordinates:')\n",
    "print('x = {:~P}'.format(test_stand_x_cace.to(ureg.m)))\n",
    "print('y = {:~P}'.format(test_stand_y_cace.to(ureg.m)))\n",
    "print('z = {:~P}'.format(test_stand_z_cace.to(ureg.m)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Test stand coordinate system\n",
    "To get the IP position in the test stand coordinate system, we take the negative of these coordinates and then rotate to the test stand axes.\n",
    "\n",
    "- The origin is at the center of the test stand structure.\n",
    "- The test stand x-axis is the negative y<sub>CACE</sub> direction. \n",
    "- The test stand y-axis is the negative x<sub>CACE</sub> direction. \n",
    "- The test stand z-axis is the negative z<sub>CACE</sub> direction. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "IP position in test stand coordinate system:\n",
      "x = (2.386 ± 0.023) m\n",
      "y = (0.000 ± 0.025) m\n",
      "z = (83.009 ± 0.006) m\n"
     ]
    }
   ],
   "source": [
    "ip_x_ts = -(-test_stand_y_cace)\n",
    "ip_y_ts = -(-test_stand_x_cace)\n",
    "ip_z_ts = -(-test_stand_z_cace)\n",
    "print('IP position in test stand coordinate system:')\n",
    "print('x = {:~P}'.format(ip_x_ts.to(ureg.m)))\n",
    "print('y = {:~P}'.format(ip_y_ts.to(ureg.m)))\n",
    "print('z = {:~P}'.format(ip_z_ts.to(ureg.m)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## ATLAS coordinate system\n",
    "The ATLAS coordinate system is defined in the following way.\n",
    "\n",
    "- The origin is the ATLAS IP.\n",
    "- The z-axis is along the beam axis, counterclockwise around the LHC.\n",
    "- The x-axis is horizontal, towards the center of the LHC ring.\n",
    "- The resulting y-axis is *mostly* vertically upwards.\n",
    "\n",
    "The ATLAS axes are much more troublesome, since they are defined by the beam axis, which is not horizontal. As shown earlier in the diagram with the different coordinate systems, the beam has a 1.236% downward slope in the +*z*<sub>ATLAS</sub> direction. Here are the calculations:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [],
   "source": [
    "import uncertainties, uncertainties.umath"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [],
   "source": [
    "beam_slope = uncertainties.ufloat(1.236 / 100.0, (0.001 / 100.0) / math.sqrt(12.0))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Test stand center position in ATLAS coordinate system:\n",
      "x = (0.000 ± 0.025) m\n",
      "y = (82.973 ± 0.006) m\n",
      "z = (-3.412 ± 0.023) m\n"
     ]
    }
   ],
   "source": [
    "beam_angle = uncertainties.umath.atan(beam_slope)\n",
    "cos = uncertainties.umath.cos(beam_angle)\n",
    "sin = uncertainties.umath.sin(beam_angle)\n",
    "\n",
    "test_stand_x_atlas =  test_stand_x_cace\n",
    "test_stand_y_atlas =  test_stand_z_cace * cos - test_stand_y_cace * sin\n",
    "test_stand_z_atlas = -test_stand_y_cace * cos - test_stand_z_cace * sin\n",
    "\n",
    "print('Test stand center position in ATLAS coordinate system:')\n",
    "print('x = {:~P}'.format(test_stand_x_atlas.to(ureg.m)))\n",
    "print('y = {:~P}'.format(test_stand_y_atlas.to(ureg.m)))\n",
    "print('z = {:~P}'.format(test_stand_z_atlas.to(ureg.m)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "ATLAS coordinates are more commonly expressed in r, eta, and phi:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Test stand center position in ATLAS coordinate system:\n",
      "r = (83.043 ± 0.006) m\n",
      "eta = (-0.04111 ± 0.00028) (equivalent to theta = (92.355 ± 0.016) deg)\n",
      "phi = (90.000 ± 0.018) deg\n"
     ]
    }
   ],
   "source": [
    "r_in_meters = uncertainties.umath.hypot(uncertainties.umath.hypot(test_stand_x_atlas.to(ureg.m).magnitude, test_stand_y_atlas.to(ureg.m).magnitude), test_stand_z_atlas.to(ureg.m).magnitude)\n",
    "test_stand_r_atlas = (r_in_meters.nominal_value * ureg.m).plus_minus(r_in_meters.std_dev)\n",
    "\n",
    "cos_theta = test_stand_z_atlas.to(ureg.m) / test_stand_r_atlas.to(ureg.m)\n",
    "test_stand_cos_theta_atlas = uncertainties.ufloat(cos_theta.value, cos_theta.std_dev)\n",
    "theta_in_radians = uncertainties.umath.acos(test_stand_cos_theta_atlas)\n",
    "test_stand_theta_atlas = (theta_in_radians.nominal_value * ureg.rad).plus_minus(theta_in_radians.std_dev)\n",
    "\n",
    "eta = -uncertainties.umath.log(uncertainties.umath.tan(theta_in_radians / 2.0))\n",
    "test_stand_eta_atlas = ureg.Quantity(eta.nominal_value).plus_minus(eta.std_dev)\n",
    "\n",
    "phi_in_radians = uncertainties.umath.atan2(test_stand_y_atlas.to(ureg.m).magnitude, test_stand_x_atlas.to(ureg.m).magnitude)\n",
    "test_stand_phi_atlas = (phi_in_radians.nominal_value * ureg.rad).plus_minus(phi_in_radians.std_dev)\n",
    "\n",
    "print('Test stand center position in ATLAS coordinate system:')\n",
    "print('r = {:~P}'.format(test_stand_r_atlas.to(ureg.m)))\n",
    "print('eta = {:~P}(equivalent to theta = {:~P})'.format(test_stand_eta_atlas, test_stand_theta_atlas.to(ureg.degree)))\n",
    "print('phi = {:~P}'.format(test_stand_phi_atlas.to(ureg.degree)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Cavern height\n",
    "\n",
    "The last thing needed is the cavern height so we can get a good estimate of the depth of rock and concrete between the IP and the test stand. The cavern vault was surveyed in April 2019 (https://edms.cern.ch/ui/file/2149688/1/ATLvault_24042019.pdf). This report mentions that the nominal vaulted ceiling (20 m radius) has a maximum vertical height of (3.52 + 20) m = 23.52 m above the IP, so (11.37 + 23.52) m = 34.89 m above the floor. The center of the cavern and vault is displaced horizontally by 1.7 m from the IP. They actually use yet another coordinate system, but this is in the -x direction in the CACE and ATLAS frames, and in the +y direction in the test stand frame. The average deviation of their measurements (both in 2008 and 2019) from the nominal ceiling was about 4 cm in the vault's radial direction. This provides a good estimate for the total uncertainty in the thickness of rock and concrete between the IP and the test stand."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
