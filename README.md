Everything is in the calculation.ipynb notebook. The images don't seem to show up on GitLab, but it should work fine if you view it locally or online on Binder:

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.cern.ch%2Fmproffit%2FTestStandIPCalculation/1d9851d5ffc4fbf8049265bf5f4622dcba97ffed?filepath=calculation.ipynb)
